from unittest import TestCase
from src import app as flask_app

class AppTestCase(TestCase):
    def test_color(self):
        # list of valid colors allowed
        valid_colors = ['red', 'green', 'blue']

        # test that color function returns a value that is in the list of valid colors
        self.assertIn(flask_app.color(), valid_colors)
